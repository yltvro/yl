
import openpyxl
from openpyxl import load_workbook,Workbook
import os
import random

def get_max_data_rows(sheet):
    """获取每一列中存在数据的最大行数"""
    max_column = sheet.max_column
    max_row = sheet.max_row
    column_max_row = {
        col: max((row for row in range(1, max_row + 1) if sheet.cell(row=row, column=col).value), default=1)
        for col in range(1, max_column + 1)
    }
    return column_max_row

def get_random_rows(max_data_row, n):
    """生成不重复的随机行号列表"""
    if max_data_row <= 1:
        return []
    row_range = range(1, max_data_row)
    n = min(max_data_row - 1, max(n, 2))  # 确保至少选择两行且不超过实际行数
    return random.sample(row_range, n)

def construct_daily_report(sheet, cols, column_max_row, n):
    """根据随机列和行号构建日报内容"""
    var_daily = []
    for index, col in enumerate(cols):
        max_data_row = column_max_row[col]
        rowlist = get_random_rows(max_data_row, n)
        varlue = sheet.cell(row=1, column=col).value
        if varlue:
            var_daily.append(f'{varlue}:（')
        for r in rowlist:
            varr = sheet.cell(row=1 + r, column=col).value
            if varr:
                var_daily.append(varr)
        if var_daily and var_daily[-1] and var_daily[-1][-1] == '、':
            var_daily[-1] = var_daily[-1][:-1]
        var_daily.append('）；' if index < len(cols) - 1 else '）。')
    return ''.join(var_daily)

def main():
    # 获取当前脚本所在目录
    current_directory = os.path.dirname(os.path.abspath(__file__))

    # 构建文件路径
    input_file_path = os.path.join(current_directory, 'in.xlsx')
    output_file_path = os.path.join(current_directory, 'out1.xlsx')

    # 读取excel文档
    wb = load_workbook(input_file_path)
    sheet = wb['Sheet1']

    # 检查是否存在输出文件，如果不存在则创建一个新的
    if not os.path.exists(output_file_path):
        wb1 = Workbook()
        sheet1 = wb1.active
        sheet1.title = '工作表1'
    else:
        wb1 = load_workbook(output_file_path)
        sheet1 = wb1['工作表1']

    # 获取每一列中存在数据的最大行数
    column_max_row = get_max_data_rows(sheet)
    max_columns = sheet.max_column

    # 提示输入需要的随机数数量
    while True:
        try:
            n = int(input(f"请输入需要的随机数数量（最大值为 {max_columns}）: "))
            if 0 < n <= max_columns:
                break
            else:
                print(f"请输入一个大于0且小于等于 {max_columns} 的正整数。")
        except ValueError:
            print("输入无效，请输入一个正整数。")

    # 生成随机列并构建日报内容
    for k in range(1, sheet.max_row + 1):
        cols = random.sample(range(1, max_columns + 1), n)
        daily_report = construct_daily_report(sheet, cols, column_max_row, n)
        print(daily_report)
        sheet1.cell(row=k, column=2).value = daily_report

    wb1.save(output_file_path)

if __name__ == "__main__":
    main()