#!/bin/bash
stty -echo
endpoint="162.159.192.1:2408"
read -p "请输入端点IP和端口(默认${endpoint}): " input
if [ -n "$input" ]; then
  endpoint="$input"
fi
warp-cli clear-custom-endpoint
warp-cli set-custom-endpoint "$endpoint"
echo "当前端点已经设置为 $endpoint"
stty echo
read -p "按回车键继续..."