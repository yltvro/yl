import sys
import os
import re
from PyQt5.QtWidgets import (QApplication, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QComboBox, QProgressBar, QFileDialog, QMessageBox, QPlainTextEdit)
from PyQt5.QtCore import QThread, pyqtSignal, QTimer
import yt_dlp

class FormatFetchThread(QThread):
    fetch_progress = pyqtSignal(int, int)  # Signal for progress (current, total)
    fetch_finished = pyqtSignal(list)      # Signal for finished fetching with formats list

    def __init__(self, url):
        super().__init__()
        self.url = url

    def run(self):
        self.fetch_progress.emit(0, 100)
        try:
            ydl_opts = {
                'listformats': True,
            }
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                info_dict = ydl.extract_info(self.url, download=False)
                formats = info_dict.get('formats', [])

                video_formats = []
                audio_formats = []

                total_formats = len(formats)
                current_progress = 0

                for idx, format in enumerate(formats, start=1):
                    if 'vcodec' in format and format['vcodec'] != 'none':
                        video_formats.append(format)
                    elif 'acodec' in format and format['acodec'] != 'none':
                        audio_formats.append(format)

                    current_progress = idx * 100 // total_formats
                    self.fetch_progress.emit(current_progress, 100)

                self.fetch_finished.emit([video_formats, audio_formats])

        except yt_dlp.utils.DownloadError as e:
            error_message = str(e)
            QMessageBox.critical(None, 'Error', f'Error fetching formats: {error_message}')

class DownloadThread(QThread):
    progress = pyqtSignal(int)
    message = pyqtSignal(str)
    finished = pyqtSignal()

    def __init__(self, url, output_path, video_format, audio_format):
        super().__init__()
        self.url = url
        self.output_path = output_path
        self.video_format = video_format
        self.audio_format = audio_format

    def run(self):
        try:
            self.progress.emit(0)
            ydl_opts = {
                'outtmpl': os.path.join(self.output_path, '%(title)s.%(ext)s'),
                'progress_hooks': [self.progress_hook],
                'merge_output_format': 'mp4',
                'add_metadata': True,
                'embed_thumbnail': True,
                'embed_subs': True,
                'format': f'{self.video_format}+{self.audio_format}' if self.audio_format else self.video_format,
                'ffmpeg_location': 'D:\\ffmpeg\\bin\\ffmpeg.exe',  # 指定你的 ffmpeg 路径
            }
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                ydl.download([self.url])
        except yt_dlp.utils.DownloadError as e:
            error_message = str(e)
            self.message.emit(f'Download error: {error_message}')
        self.finished.emit()

    def progress_hook(self, d):
        if d['status'] == 'downloading':
            message = d.get('_percent_str', '')
            self.message.emit(message)

class VideoDownloader(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Downloader')
        self.setGeometry(100, 100, 500, 100)

        layout = QVBoxLayout()

        self.url_label = QLabel('Video URL:')
        layout.addWidget(self.url_label)

        self.url_input = QLineEdit(self)
        layout.addWidget(self.url_input)

        self.get_formats_button = QPushButton('Get Formats', self)
        self.get_formats_button.clicked.connect(self.start_fetch_formats)
        layout.addWidget(self.get_formats_button)

        self.output_path_label = QLabel('Output Path:')
        layout.addWidget(self.output_path_label)

        self.output_path_button = QPushButton('Select Output Folder', self)
        self.output_path_button.clicked.connect(self.select_output_folder)
        layout.addWidget(self.output_path_button)

        self.video_format_label = QLabel('Video Format:')
        layout.addWidget(self.video_format_label)

        self.video_format_combo = QComboBox(self)
        layout.addWidget(self.video_format_combo)

        self.audio_format_label = QLabel('Audio Format:')
        layout.addWidget(self.audio_format_label)

        self.audio_format_combo = QComboBox(self)
        layout.addWidget(self.audio_format_combo)

        self.download_button = QPushButton('Download', self)
        self.download_button.clicked.connect(self.start_download)
        layout.addWidget(self.download_button)

        self.progress_bar = QProgressBar(self)
        layout.addWidget(self.progress_bar)

        self.status_label = QLabel('Ready')  # Status label to display current operation
        layout.addWidget(self.status_label)

        self.log_text = QPlainTextEdit(self)
        self.log_text.setReadOnly(True)
        layout.addWidget(self.log_text)

        self.setLayout(layout)

    def select_output_folder(self):
        self.output_path = QFileDialog.getExistingDirectory(self, 'Select Output Folder')
        if self.output_path:
            self.output_path_button.setText(self.output_path)

    def start_fetch_formats(self):
        url = self.url_input.text()
        if not url:
            QMessageBox.warning(None, 'URL Missing', 'Please enter a valid URL.')
            return

        self.status_label.setText('Fetching Formats...')
        self.fetch_thread = FormatFetchThread(url)
        self.fetch_thread.fetch_finished.connect(self.populate_format_combos)
        self.fetch_thread.start()

        self.get_formats_button.setEnabled(False)
        self.download_button.setEnabled(False)

    def populate_format_combos(self, formats):
        video_formats, audio_formats = formats

        # Populate video format combo box
        self.video_format_combo.clear()
        self.video_format_combo.addItem('Best Video Format (MP4)', 'bestvideo[ext=mp4]')
        for format in video_formats:
            format_name = format.get('format_note', format['format_id'])
            self.video_format_combo.addItem(format_name, format['format_id'])

        # Populate audio format combo box
        self.audio_format_combo.clear()
        self.audio_format_combo.addItem('Best Audio Format (M4A)', 'bestaudio[ext=m4a]')
        for format in audio_formats:
            format_name = format.get('format_note', format['format_id'])
            self.audio_format_combo.addItem(format_name, format['format_id'])

        self.get_formats_button.setEnabled(True)
        self.download_button.setEnabled(True)
        self.status_label.setText('Ready')

    def start_download(self):
        url = self.url_input.text()
        output_path = self.output_path
        video_format = self.video_format_combo.currentData()
        audio_format = self.audio_format_combo.currentData()

        if not url:
            QMessageBox.warning(None, 'URL Missing', 'Please enter a valid URL.')
            return

        self.status_label.setText('Downloading...')
        self.download_thread = DownloadThread(url, output_path, video_format, audio_format)
        self.download_thread.progress.connect(self.update_progress)
        self.download_thread.message.connect(self.update_log)
        self.download_thread.finished.connect(self.download_finished)
        self.download_thread.start()
        self.download_button.setEnabled(False)
        self.download_button.setText('Downloading...')

        # Create a timer to update progress bar during download
        self.timer = QTimer()
        self.timer.timeout.connect(self.check_download_progress)
        self.timer.start(1000)  # Update every second

    def check_download_progress(self):
        if self.download_thread.isFinished():
            self.timer.stop()

    def update_progress(self, percent):
        self.progress_bar.setValue(percent)

    def update_log(self, message):
        self.log_text.setPlainText(message)

    def download_finished(self):
        self.progress_bar.setValue(100)
        self.download_button.setEnabled(True)
        self.download_button.setText('Download')
        self.status_label.setText('Download Finished')
        QMessageBox.information(None, 'Download Finished', 'Download finished successfully!')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = VideoDownloader()
    ex.show()
    sys.exit(app.exec_())
