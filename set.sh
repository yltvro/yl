#!/bin/bash

# 禁用回显
stty -echo

# 设置默认端点
endpoint="[2606:4700:d0::77e3:5294:2faa:d03f]:955"

# 提示用户输入端点IP和端口
read -p "请输入端点IP和端口(默认${endpoint}): " input
if [ -n "$input" ]; then
  endpoint="$input"
fi

# 设置自定义端点
warp-cli clear-custom-endpoint
warp-cli set-custom-endpoint "$endpoint"

# 输出当前端点
echo "当前端点已经设置为 $endpoint"

# 启用回显
stty echo

# 暂停等待用户输入
read -p "按回车键继续..."